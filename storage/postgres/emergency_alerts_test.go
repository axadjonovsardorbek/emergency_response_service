package postgres

import (
	"database/sql"
	cf "emergency_response-service/config"
	"fmt"
	"testing"

	er "emergency_response-service/genproto/emergency_response"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func setupTestDB(t *testing.T) (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	return db, mock
}

func TestCreate(t *testing.T) {
    db, mock := setupTestDB(t)
    defer db.Close()

    repo := NewEmergencyAlertsRepo(db)
    alertID := uuid.New().String()
    req := &er.AlertsCreateReq{
        Type:         "test_type",
        Message:      "test_message",
        AffectedArea: "(12.3456,78.9012)",
        IssuedAt:     "2024-07-01 12:00:00",
    }

    mock.ExpectQuery(`INSERT INTO emergency_alerts\(id, type, message, affected_area, issued_at\) VALUES \(\$1, \$2, \$3, \$4, \$5\) RETURNING id, type, message, affected_area, issued_at`).
        WithArgs(alertID, req.Type, req.Message, req.AffectedArea, req.IssuedAt).
        WillReturnRows(sqlmock.NewRows([]string{"id", "type", "message", "affected_area", "issued_at"}).
            AddRow(alertID, req.Type, req.Message, req.AffectedArea, req.IssuedAt))

    alert, err := repo.Create(req)

    assert.NoError(t, err)
    assert.NotNil(t, alert)
    assert.Equal(t, alertID, alert.Id)
    assert.Equal(t, req.Type, alert.Type)
    assert.Equal(t, req.Message, alert.Message)
    assert.Equal(t, req.AffectedArea, alert.AffectedArea)
    assert.Equal(t, req.IssuedAt, alert.IssuedAt)

    if err := mock.ExpectationsWereMet(); err != nil {
        t.Errorf("there were unfulfilled expectations: %s", err)
    }
}

func TestGetById(t *testing.T) {
	config := cf.Load()
	conn := fmt.Sprintf("host=%s user=%s dbname=%s password=%s port=%d sslmode=disable",
		config.DB_HOST, config.DB_USER, config.DB_NAME, config.DB_PASSWORD, config.DB_PORT)
	// Initialize your database connection here
	db, err := sql.Open("postgres", conn)
	if err != nil {
		t.Fatalf("failed to connect to the database: %v", err)
	}
	defer db.Close()

	repo := NewEmergencyAlertsRepo(db)
	alertID := "77777777-7777-7777-7777-777777777777"
	byId := &er.ById{Id: alertID}

	alert, err := repo.GetById(byId)

	assert.NoError(t, err)
	assert.NotNil(t, alert)
	assert.Equal(t, alertID, alert.Alert.Id)
	assert.Equal(t, "updated_type", alert.Alert.Type)
	assert.Equal(t, "updated_message", alert.Alert.Message)
	assert.Equal(t, "(34.0522,-118.2438)", alert.Alert.AffectedArea)
	assert.Equal(t, "2024-06-30T13:00:00Z", alert.Alert.IssuedAt)
}

func TestGetAll(t *testing.T) {
	db, mock := setupTestDB(t)
	defer db.Close()

	repo := NewEmergencyAlertsRepo(db)
	filter := &er.Filter{Limit: 10, Offset: 0}

	mock.ExpectQuery("SELECT COUNT\\(1\\) FROM emergency_alerts WHERE deleted_at=0").
		WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(2))

	mock.ExpectQuery(`SELECT id, type, message, affected_area, issued_at FROM emergency_alerts WHERE deleted_at = 0 LIMIT \$1 OFFSET \$2`).
		WithArgs(filter.Limit, filter.Offset).
		WillReturnRows(sqlmock.NewRows([]string{"id", "type", "message", "affected_area", "issued_at"}).
			AddRow("77777777-7777-7777-7777-777777777777", "updated_type", "updated_message", "(34.0522,-118.2438)", "2024-06-30 13:00:00").
			AddRow("88888888-8888-8888-8888-888888888888", "public health", "COVID-19 outbreak", "(34.0522,-118.2438)", "2024-06-30 13:58:29.498919"))

	alerts, err := repo.GetAll(filter)

	assert.NoError(t, err)
	assert.NotNil(t, alerts)
	assert.Len(t, alerts.Alerts, 2)

	// Check the first alert
	assert.Equal(t, "77777777-7777-7777-7777-777777777777", alerts.Alerts[0].Id)
	assert.Equal(t, "updated_type", alerts.Alerts[0].Type)
	assert.Equal(t, "updated_message", alerts.Alerts[0].Message)
	assert.Equal(t, "(34.0522,-118.2438)", alerts.Alerts[0].AffectedArea)
	assert.Equal(t, "2024-06-30 13:00:00", alerts.Alerts[0].IssuedAt)

	// Check the second alert
	assert.Equal(t, "88888888-8888-8888-8888-888888888888", alerts.Alerts[1].Id)
	assert.Equal(t, "public health", alerts.Alerts[1].Type)
	assert.Equal(t, "COVID-19 outbreak", alerts.Alerts[1].Message)
	assert.Equal(t, "(34.0522,-118.2438)", alerts.Alerts[1].AffectedArea)
	assert.Equal(t, "2024-06-30 13:58:29.498919", alerts.Alerts[1].IssuedAt)

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestUpdate(t *testing.T) {
	config := cf.Load()
	conn := fmt.Sprintf("host=%s user=%s dbname=%s password=%s port=%d sslmode=disable",
		config.DB_HOST, config.DB_USER, config.DB_NAME, config.DB_PASSWORD, config.DB_PORT)
	// Initialize your database connection here
	db, err := sql.Open("postgres", conn)
	if err != nil {
		t.Fatalf("failed to connect to the database: %v", err)
	}
	defer db.Close()

	repo := NewEmergencyAlertsRepo(db)

	id := "88888888-8888-8888-8888-888888888888"

	if err != nil {
		t.Fatalf("failed to create test alert: %v", err)
	}

	// Prepare the update request
	// req := &er.AlertsUpdateReq{
	// 	Id: id,
	// 	Alert: &er.AlertsCreateReq{
	// 		Type:         "updated_type",
	// 		Message:      "updated_message",
	// 		AffectedArea: "(34.0522,-118.2438)",
	// 		IssuedAt:     "2024-06-30T13:00:00Z",
	// 	},
	// }
	req := &er.AlertsUpdateReq{
		Id: id,
		Alert: &er.AlertsCreateReq{
			Type:         "public health",
			Message:      "COVID-19 outbreak",
			AffectedArea: "(34.0522,-118.2438)",
			IssuedAt:     "2024-06-30T13:00:00Z",
		},
	}

	// Call the Update method
	updatedAlert, err := repo.Update(req)
	if err != nil {
		t.Fatalf("failed to update alert: %v", err)
	}

	// Check if the updatedAlert is not nil
	assert.NotNil(t, updatedAlert, "updatedAlert should not be nil")

	// Check if the updated fields match
	assert.Equal(t, req.Alert.Type, updatedAlert.Type)
	assert.Equal(t, req.Alert.Message, updatedAlert.Message)
	assert.Equal(t, req.Alert.AffectedArea, updatedAlert.AffectedArea)
	assert.Equal(t, req.Alert.IssuedAt, updatedAlert.IssuedAt)
}

func TestDelete(t *testing.T) {
	db, mock := setupTestDB(t)
	defer db.Close()

	repo := NewEmergencyAlertsRepo(db)
	id := &er.ById{Id: "77777777-7777-7777-7777-777777777777"}

	mock.ExpectExec(`UPDATE emergency_alerts SET deleted_at = EXTRACT\(EPOCH FROM NOW\(\)\) WHERE id = \$1 AND deleted_at = 0`).WithArgs(id.Id).
		WillReturnResult(sqlmock.NewResult(1, 1))

	void, err := repo.Delete(id)
	assert.NoError(t, err)
	assert.NotNil(t, void)
}
